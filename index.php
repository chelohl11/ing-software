<?php

interface Armor
{
    public function protectDamage($damage);
}

class Unit{

    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}


class Soldier extends Unit{
    protected $armor;

    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function setArmor(Armor $armor)
    {
        $this->armor = $armor;
    }


}

class BronzerArmor implements Armor{
    public function protectDamage($damage)
    {
        return $damage / 2;
    }
}

class SilverArmor implements Armor{
    public function protectDamage($damage)
    {
        return $damage / 5;
    }
}

$armor = new BronzerArmor();

$james = new Soldier('James');

$james->setArmor($armor);

$silverArmor = new SilverArmor();

$james->setArmor($silverArmor);

